# Semantic Ledger Technology - Ledger environment

The ledger environment is the ledger supporting the Semantic Ledger technology. The Semantic Ledger platform connects to this layer to publish and read data from the distributed ledger. In this case, the supporting ledger-environment is **BigchainDB**. BigchainDB consists of several components:

* MongoDB (+ Mongo Express)
* Tendermint
* BigchainDB

This repository was authored by Jacco Spek. It is licensed under Apache 2.0, copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research.

### MongoDB
MongoDB is the nosql database supporting BigchainDB. This is where all the ledger-data is stored.

**Note:** MongoDB is licensed under a ["Server Side Public License"](https://www.mongodb.com/licensing/server-side-public-license). In narrow conditions, this license might trigger a copyleft requirement. For more information, see section 13 of the license, as well as the license's [FAQ](https://www.mongodb.com/licensing/server-side-public-license/faq).

### Mongo Express
Mongo Express is a web-based administrative interface for MongoDB.
**Note:** As [stated on its repository](https://github.com/mongo-express/mongo-express#not-tested),
Mongo Express is meant for development purposes only.

### Tendermint
Tendermint is the "distributed byzantine-fault tolerant state machine" providing BigchainDB's consensus mechanism and distributed architecture.

### BigchainDB
The core distributed ledger-technology. Managing ownership and validating transactions. BigchainDB provides the API for the semantic ledger platform to connect to. 

## Requirements
The package was tested on Ubuntu 16.04 LTS
* Docker (v17.05+)
* Docker-compose (v1.10+)

A known issue for Windows relates to mounting a volume for the mongo service. For development purposes, this mounting could be disabled, although it could cause data loss once the mongo container is removed.

## Configure, building and running the environdment

### Configure
All configuration happens in the `docker-compose.yaml` file

1. Create a folder for MongoDB to store its data, and configure this as volume in the `mongo` service. 
Make sure the docker-containers can access (read- and write) to/from this folder. Per default, the docker-compose file 
expects `./mongo` as MongoDB folder
2. Create a folder for Tendermint to store its data, and configure this as volume in the tendermint services 
(`tendermint-init`, `tendermint-nodeid` and `tendermint`). Make sure the docker-containers can access 
(read- and write) to/from this folder. Per default, the docker-compose file expects `./tm` as Tendermint folder
3. Look over the rest of the `docker-compose.yaml` file to see if the rest of the options are set as desired (ports, credentials etc.)

### Build
To build the docker-images, run: `docker-compose build`.
### Run
When the configuration is as desired, and building has succeeded, the ledger-environment can be started from the root directoy through docker-compose:
`docker-compose up -d`
### Stopping, and resetting the node
To stop the node, run `docker-compose down`. To start the node again, simply run `docker-compose up -d` again. 
To start a fresh node, delete the mongo and tendermint folder contents and then run `docker-compose up -d`

<!-- TODO update below -->
## Next steps
When the ledger is running, the semantic ledger technology can be further setup by starting the following services:
<!-- TODO update links -->
1. [SHACL Validator API](https://gitlab.com/calculemus-flint/semantic-ledger-ecosystem/shacl-validator)
2. [Semantic Ledger Platform](https://gitlab.com/calculemus-flint/semantic-ledger-ecosystem/semantic-ledger-platform)

The SHACL Validator API facilitates semantic validation of (semantic) data against rules and constraints (SHACL).

The Semantic Ledger Platform is the API that domain-specific Semantic Ledger applications communicate with.

Finally, when these services are up-and-running, the domain-specific applications can be designed and implemented to  communicate with the ledger.
The function of these applications is to translate user-specific questions into semantically structured calls towards the Semantic Ledger API.